#!/usr/bin/env python

import os.path
from setuptools import setup, find_packages

setup(
    name = 'TracLDAPEmailResolver',
    version = '0.1',
    description = 'LDAP Email resolver plugin for Trac',
    author = "Sandro Santilli",
    author_email = 'strk@kbt.io',
    url = 'https://git.osgeo.org/gitea/sac/TracLDAPEmailResolverPlugin',
    download_url = 'https://git.osgeo.org/gitea/sac/TracLDAPEmailResolverPlugin/archive/master.zip',
    packages=find_packages(exclude=['*.tests*']),
    entry_points = {
        'trac.plugins': [
            'ldapmail = ldapmail',
        ]
    },
)
