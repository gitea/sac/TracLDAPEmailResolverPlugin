# Adapted from https://trac.edgewall.org/wiki/TracDev/PluginDevelopment/ExtensionPoints/trac.notification.api.IEmailAddressResolver#Examples
import ldap
import ldap.filter

from trac.core import *
from trac.notification.api import IEmailAddressResolver
from trac.config import (ExtensionOption)

class LdapEmailResolver(Component):
    """Gets the email address of a user from a LDAP database."""

    implements(IEmailAddressResolver)

    # IEmailAddressResolver methods

    def get_address_for_session(self, sid, authenticated):
        address = None

        config = self.config['ldapmail']
        uri = config.get('uri')
        if not uri:
            raise TracError("Please configure 'uri' under [ldapmail] in trac.ini")
        base = config.get('base')
        if not base:
            raise TracError("Please configure 'base' under [ldapmail] in trac.ini")

        self.log.debug("ldapmail.uri=%r", uri)
        self.log.debug("ldapmail.base=%r", base)

        scope = ldap.SCOPE_ONELEVEL
        filter = 'uid=' + ldap.filter.escape_filter_chars(sid)

        try:
            ld = ldap.initialize(uri)
            ld.simple_bind_s()
        except Exception, e:
            raise TracError(str(e) + " -- had you set ldapmail.uri in trac.ini ?")

        for dn, entry in ld.search_s(base, scope, filterstr = filter,
                                     attrlist = ['mail']):
            self.log.debug("ldapmail match %r", entry)
            if 'mail' in entry:
                address = entry['mail'][0]
        ld.unbind_s()

        return address
