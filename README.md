This is a [Trac](https://trac.edgewall.org/) (1.2.1+) plugin to
resolve user emails via a LDAP lookup.

Website: https://git.osgeo.org/gitea/sac/TracLDAPEmailResolverPlugin

NOTE: it requires Trac 1.2.1
([r15356 or later](https://trac.edgewall.org/changeset/15356))

It was tested to work with Trac-1.4.4

# Install

From local source:

    python setup.py install

From the net:

    pip install https://git.osgeo.org/gitea/sac/TracLDAPEmailResolverPlugin/archive/master.zip

# Enable

Add "LdapEmailResolver" to the `email_address_resolvers` variable under
the [notification] section, ie:

    [notification]
    email_address_resolvers = SessionEmailResolver, LdapEmailResolver

# Configure

Configure LDAP uri and search base under the [ldapmail] section, ie:

    [ldapmail]
    uri = ldaps://ldap.corp.com:636
    base = ou=People,dc=corp,dc=com
